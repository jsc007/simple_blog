class PostsController < ApplicationController
  def index
    @user = User.find_by(id: session[:user_id])
    @posts = Post.includes(:user).references(:user).all
  end

  def new
    @post = Post.new
  end

  def create
    post = Post.new(post_params)

    user = User.find_by(id: session[:user_id])
    post.user_id = user.id

    if post.save
      redirect_to posts_path
    else
      @post = Post.new
      flash.now[:alert] = "Couldn't save the post!"
      render :new, status: :unprocessable_entity
    end
  end

  def show
    @post = Post.find(params[:id])
  end
  
  def edit
    @post = Post.find(params[:id])
  end

  def update
    user = User.find_by(id: session[:user_id])

    # make sure user is the author
    post = Post.find(params[:id])
    if post.user_id != user.id
      redirect_to post_path(post), alert: "Unauthorized!"
    elsif post.update(title: params[:post][:title], body: params[:post][:body])
      redirect_to post_path(post)
    else
      flash.now[:alert] = "Couldn't save the post!"
      render :edit, status: :unprocessable_entity
      return
    end
  end

  def destroy
    @post= Post.find(params[:id])
    @post.destroy()
   
    redirect_to root_path
  end

  private 
    def post_params
      params.require(:post).permit(:title, :body)
    end
end
