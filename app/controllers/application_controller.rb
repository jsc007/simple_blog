class ApplicationController < ActionController::Base
    before_action :require_login

    private

    def require_login
        @user = User.find_by(id: session[:user_id])
        redirect_to new_login_path unless @user
    end
end
