class LoginsController < ApplicationController
  skip_before_action :require_login, only: [:new, :create]
  
  def new
    user = User.find_by(id: session[:user_id])
    redirect_to posts_path if user
    @user = User.new
  end

  def create
    @user = User.find_by(login_params)
    if @user
      session[:user_id] = @user.id
      redirect_to posts_path
    else
      @user = User.new
      flash.now[:alert] = "Email or password is wrong!"
      render :new, status: :unprocessable_entity
    end
  end

  def destroy 
    session.clear
    redirect_to root_url, notice: "Logged out."
  end

  private
    def login_params
      params.require(:user).permit(:email, :password)
    end
end
