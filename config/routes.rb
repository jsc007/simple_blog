Rails.application.routes.draw do
  get 'logout', to: 'logins#destroy', as: :logout

  resources :logins, only: [:new, :create]
  resources :registrations, only: [:new, :create]
  resources :posts

  root "posts#index"
end
